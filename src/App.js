import React from "react";
import "./App.css";
import TicTacToe2Players from "./components/TicTacToe2Players";
import TicTacToeVsComputer from "./components/TicTacToeVsComputer";

const TWOPLAYERS = "TWOPLAYERS";
const VSCOMPUTER = "VSCOMPUTER";

function App() {
  const [state, setState] = React.useState("TWOPLAYERS");

  function onSelectGameMode(newValue) {
    alert("Select game modw");
    console.log("game mode: ", state);
    if (newValue !== state) {
      console.log("changing to ", newValue);
      setState(newValue);
    } else {
      console.log("game mode is the same as selected");
    }
  }

  console.log("Game mode ", state);
  return (
    <div className="app">
      <div>
        <button onClick={() => onSelectGameMode("TWOPLAYERS")}>
          2 players
        </button>
        <button onClick={() => onSelectGameMode("VSCOMPUTER")}>
          Vs Computer
        </button>
      </div>
      <div className="appGame">
        {state === "TWOPLAYERS" && <TicTacToe2Players />}
        {state === "VSCOMPUTER" && <TicTacToeVsComputer />}
      </div>
    </div>
  );
}

export default App;
